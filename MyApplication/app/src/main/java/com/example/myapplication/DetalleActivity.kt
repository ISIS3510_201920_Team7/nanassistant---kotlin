package com.example.myapplication

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.myapplication.models.PeticionesDentroDeCasa
import com.example.myapplication.models.PeticionesFueraDeCasa
import kotlinx.android.synthetic.main.activity_detalle.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore

class DetalleActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    private lateinit var db : FirebaseFirestore

    private var isConnected : Boolean = true

    private var valido : Boolean = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalle)
        db = FirebaseFirestore.getInstance()
        auth = FirebaseAuth.getInstance()
        ///etNombre.text = intent.getStringExtra("")
        if(intent.getSerializableExtra("dentrodecasa")!=null) {
            etDir1.text = intent.getStringExtra("dir1")
            //etDir2.text = intent.getStringExtra("dir2")
            etFecha.text = intent.getStringExtra("fecha")
            etHora.text = intent.getStringExtra("hora")
            etCategoria.text = intent.getStringExtra("cat")
            etDescripcion.text = intent.getStringExtra("des")
        }
        else{
            etDir1.text = intent.getStringExtra("dir1")
            etDir2.text = intent.getStringExtra("dir2")
            etFecha.text = intent.getStringExtra("fecha")
            etHora.text = intent.getStringExtra("hora")
            etCategoria.text = intent.getStringExtra("cat")
            etDescripcion.text = intent.getStringExtra("des")
        }

        btAtras.setOnClickListener {
            /*checkConnectivity()
            if(isConnected==true){
                //traerServicios()
            }
            */
            val intent  = Intent(this, ListaActivity::class.java)
            startActivity(intent)

        }

        btAceptar.setOnClickListener {
            checkConnectivity()
            if(isConnected==true){
                val user = FirebaseAuth.getInstance().currentUser
                if (user != null) {
                    var idAsistente = user.uid
                    if (intent.getStringExtra("identificador") != null) {
                        //Log.e("PETICION INTENT:",intent.getSerializableExtra("fueradecasa").toString())
                        /*
                        var reference = db.collection("Peticiones").document(intent.getStringExtra("identificador"))
                        var peticion = intent.getSerializableExtra("fueradecasa") as PeticionesFueraDeCasa
                        peticion.asistente = idAsistente
                        peticion.estado = "Aceptado"
                        reference.set(peticion)
                        */



                        val reference2 = db.collection("Peticiones").document(intent.getStringExtra("identificador"))

                        var dir2 = ""
                        if (intent.getStringExtra("dir2")!=null){
                            dir2=intent.getStringExtra("dir2")
                        }

                        val peticion2 = hashMapOf(
                            "asistente" to auth.currentUser?.uid.toString(),
                            "categoria" to intent.getStringExtra("cat"),
                            "correo" to intent.getStringExtra("correo"),
                            "descripcion" to intent.getStringExtra("des"),
                            "direccion1" to intent.getStringExtra("dir1"),
                            "direccion2" to dir2,
                            "estado" to "Aceptado",
                            "fecha" to intent.getStringExtra("fecha"),
                            "hora" to intent.getStringExtra("hora"),
                            "usuario" to intent.getStringExtra("usuario")
                        )

                        reference2.set(peticion2)

                        val intent  = Intent(this, ListaActivity::class.java)
                        startActivity(intent)

                    }


                }
            }
            //traerServicios()
        }


    }

    private fun checkConnectivity(){
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting
        if(!isConnected){
            Toast.makeText(this, "Por favor revisar su conexión a internet.", Toast.LENGTH_SHORT).show()
        }
    }


}
