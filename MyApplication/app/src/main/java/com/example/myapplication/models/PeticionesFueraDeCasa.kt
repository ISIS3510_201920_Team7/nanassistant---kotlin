package com.example.myapplication.models

import java.io.Serializable

class PeticionesFueraDeCasa : Serializable {

    public lateinit var categoria : String
    public lateinit var correo : String
    public lateinit var descripcion : String
    public lateinit var direccion1 : String
    public var direccion2 : String = ""
    public lateinit var fecha : String
    public lateinit var hora : String
    public lateinit var usuario : String
    public lateinit var estado : String
    public  var identificador =""
    public lateinit var asistente : String

    class PeticionesFueraDeCasa(val categoria: String, val correo : String, val descripcion: String, val direccion1 : String,
                                 val direccion2 : String, val fecha : String, val hora : String, val usuario : String, val estado : String,
                                 val identificador : String, val asistente:String ){

    }

    constructor(){

    }

}