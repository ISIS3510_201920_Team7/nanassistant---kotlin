package com.example.myapplication

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.example.myapplication.models.PeticionesDentroDeCasa
import com.example.myapplication.models.PeticionesFueraDeCasa
import com.google.android.gms.location.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore

import kotlinx.android.synthetic.main.activity_detalle2.*

class DetalleActivity2 : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var identificador: String

    private lateinit var db : FirebaseFirestore

    private var isConnected : Boolean = true

    private var valido : Boolean = true

    private var lista = arrayListOf<String>()

    private var lista2 = arrayListOf<String>()

    private var peticiones = arrayListOf<PeticionesDentroDeCasa>()

    private var peticiones2 = arrayListOf<PeticionesFueraDeCasa>()

    val PERMISSION_ID = 42

    lateinit var mFusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalle2)
        db = FirebaseFirestore.getInstance()
        auth = FirebaseAuth.getInstance()
        identificador= intent.getStringExtra("identificador")
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        etNombre.text = intent.getStringExtra("nombre")
        if(intent.getSerializableExtra("dentrodecasa")!=null) {
            etDir1.text = intent.getStringExtra("dir1")
            //etDir2.text = intent.getStringExtra("dir2")
            etFecha.text = intent.getStringExtra("fecha")
            etHora.text = intent.getStringExtra("hora")
            etCategoria.text = intent.getStringExtra("cat")
            etDescripcion.text = intent.getStringExtra("des")
        }
        else{
            etDir1.text = intent.getStringExtra("dir1")
            etDir2.text = intent.getStringExtra("dir2")
            etFecha.text = intent.getStringExtra("fecha")
            etHora.text = intent.getStringExtra("hora")
            etCategoria.text = intent.getStringExtra("cat")
            etDescripcion.text = intent.getStringExtra("des")
        }



        btCancelar2.setOnClickListener {
            checkConnectivity()
            if(isConnected==true){
                val user = FirebaseAuth.getInstance().currentUser
                if (user != null) {
                    var idAsistente = user.uid
                    if(intent.getSerializableExtra("dentrodecasa")!=null) {
                        var reference = db.collection("Peticiones").document(intent.getStringExtra("identificador"))
                        var peticion = intent.getSerializableExtra("dentrodecasa") as PeticionesDentroDeCasa
                        peticion.asistente="Sin asignar"
                        peticion.estado="Disponible"
                        reference.set(peticion)

                    }
                    else{
                        var reference = db.collection("Peticiones").document(intent.getStringExtra("identificador"))
                        /*var peticion = intent.getSerializableExtra("fueradecasa") as PeticionesFueraDeCasa
                        peticion.asistente="Sin asignar"
                        peticion.estado="Disponible"
                        reference.set(peticion)
                        */


                        //val reference2 = db.collection("Peticiones").document(intent.getStringExtra("identificador"))

                        var dir2 = ""
                        if (intent.getStringExtra("dir2")!=null){
                            dir2=intent.getStringExtra("dir2")
                        }

                        val peticion2 = hashMapOf(
                            "asistente" to "Sin asignar",
                            "categoria" to intent.getStringExtra("cat"),
                            "correo" to intent.getStringExtra("correo"),
                            "descripcion" to intent.getStringExtra("des"),
                            "direccion1" to intent.getStringExtra("dir1"),
                            "direccion2" to dir2,
                            "estado" to "disponible",
                            "fecha" to intent.getStringExtra("fecha"),
                            "hora" to intent.getStringExtra("hora"),
                            "usuario" to intent.getStringExtra("usuario")
                        )

                        reference.set(peticion2)

                        val intent  = Intent(this, ListaActivity::class.java)
                        startActivity(intent)
                    }

                }
            }
            //traerServicios()
        }

        btAtras2.setOnClickListener {
            /*checkConnectivity()
            if(isConnected==true){
                traerServicios()
            }

             */
            val intent = Intent(this, ChatActivity::class.java)
            startActivity(intent)
        }

       ivLocation.setOnClickListener(){
            checkConnectivity()
            if(isConnected){
                if(intent.getStringExtra("identificador")==null){
                    Log.e("YYYYY", "null 2")
                }

                val docRef=db.collection("localizacion").document(identificador)
                docRef.get()
                        .addOnSuccessListener { document ->
                            if (document != null) {
                                Log.e("localizacion", "DocumentSnapshot data: ${document.data}")
                                var intento = Intent(this, MapsActivity::class.java)
                                intento.putExtra("longitud",document.data!!.get("longitud").toString())
                                intento.putExtra("latitud",document.data!!.get("latitud").toString())
                                intento.putExtra("usuario",intent.getStringExtra("identificador"))
                                startActivity(intento)
                            } else {
                                Log.d("localizacion", "No such document")
                            }
                        }
                        .addOnFailureListener { exception ->
                            Log.d("localizacion", "get failed with ", exception)
                        }


            }
        }


        ivStart.setOnClickListener(){
            getLastLocation()
            var intento = Intent(this, LocationActivity::class.java)
            startActivity(intento)

        }


    }

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            return true
        }
        return false
    }
    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSION_ID
        )
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                // Granted. Start getting the location information
            }
        }
    }
    private fun isLocationEnabled(): Boolean {
        var locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        )
    }
    private fun checkConnectivity(){
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting
        if(!isConnected){
            Toast.makeText(this, "Por favor revisar su conexión a internet.", Toast.LENGTH_SHORT).show()
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {

                mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    var location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                        Log.e("UBICACION",location.latitude.toString())
                        Log.e("UBICACION",location.longitude.toString())

                        enviarCoordenadas(location.longitude,location.latitude)


                    }
                }
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient!!.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        )
    }
    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            var mLastLocation: Location = locationResult.lastLocation
           // Toast.makeText(this, mLastLocation.latitude.toString(), Toast.LENGTH_SHORT).show()
           // findViewById<TextView>(R.id.latTextView).text = mLastLocation.latitude.toString()
            //findViewById<TextView>(R.id.lonTextView).text = mLastLocation.longitude.toString()
            Log.e("UBICACION",mLastLocation.latitude.toString())
            Log.e("UBICACION",mLastLocation.longitude.toString())
            enviarCoordenadas(mLastLocation.longitude,mLastLocation.latitude)
        }
    }

    fun enviarCoordenadas(longitud:Double, latitud:Double){

        val docData = hashMapOf(
                "longitud" to longitud.toString(),
                "latitud" to latitud.toString()
        )
        val code = auth.currentUser?.uid?:""
        db.collection("localizacion").document(code)
                .set(docData)
    }
    public fun traerServicios(){
        var intento = Intent(this, ListaActivity::class.java)


        var idUsuario = "a"

        idUsuario = auth.currentUser?.uid ?: "a"


        db.collection("PeticionesDentroDeCasa").whereEqualTo("asistente","Sin asignar")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    Log.d("Encontró documento", "${document.id} => ${document.data}")
                    val nuevaPeticion : PeticionesDentroDeCasa
                    nuevaPeticion = document.toObject(PeticionesDentroDeCasa::class.java)
                    nuevaPeticion.identificador = document.id
                    peticiones.add(nuevaPeticion)
                    lista.add("\n"+"Categoria: " +nuevaPeticion.categoria+"\n" +"fecha: "+nuevaPeticion.fecha+"\n")
                    //intento.putExtra("dentrodecasa",peticiones)


                }
                db.collection("PeticionesDentroDeCasa").whereEqualTo("asistente",idUsuario)
                    .get()
                    .addOnSuccessListener { documents ->
                        for (document in documents) {
                            Log.d("Encontró documento", "${document.id} => ${document.data}")
                            val nuevaPeticion : PeticionesDentroDeCasa
                            nuevaPeticion = document.toObject(PeticionesDentroDeCasa::class.java)
                            nuevaPeticion.identificador = document.id
                            peticiones.add(nuevaPeticion)
                            lista2.add("\n"+"Categoria: " +nuevaPeticion.categoria+"\n" +"fecha: "+nuevaPeticion.fecha+"\n")
                        }
                        intento.putExtra("dentrodecasa",peticiones)
                        //intento.putExtra("ListadeSalidas", lista)


                        db.collection("PeticionesFueraDeCasa").whereEqualTo("asistente","Sin asignar")
                            .get()
                            .addOnSuccessListener { documents ->
                                for (document in documents) {
                                    Log.d("Encontró documento", "${document.id} => ${document.data}")
                                    val nuevaPeticion : PeticionesFueraDeCasa
                                    nuevaPeticion = document.toObject(PeticionesFueraDeCasa::class.java)
                                    nuevaPeticion.identificador = document.id
                                    peticiones2.add(nuevaPeticion)
                                    lista.add("\n"+"Categoria: " +nuevaPeticion.categoria+"\n" +"fecha: "+nuevaPeticion.fecha+"\n")
                                }
                                //intento.putExtra("fueradecasa",peticiones2)
                                //intentop.putExtra("ListadeSalidas", lista)


                                db.collection("PeticionesFueraDeCasa").whereEqualTo("asistente",idUsuario)
                                    .get()
                                    .addOnSuccessListener { documents ->
                                        for (document in documents) {
                                            Log.d("Encontró documento", "${document.id} => ${document.data}")
                                            val nuevaPeticion : PeticionesFueraDeCasa
                                            nuevaPeticion = document.toObject(PeticionesFueraDeCasa::class.java)
                                            nuevaPeticion.identificador = document.id
                                            peticiones2.add(nuevaPeticion)
                                            lista2.add("\n"+"Categoria: " +nuevaPeticion.categoria+"\n" +"fecha: "+nuevaPeticion.fecha+"\n")
                                        }

                                        intento.putExtra("fueradecasa",peticiones2)
                                        intento.putExtra("ListaDeSalidas", lista)
                                        intento.putExtra("ListaDeSalidas2", lista2)
                                        //intento.putExtra("ListaDeSalidas", arrayListOf("Prueba1"))

                                        startActivity(intento)
                                        finish()
                                    }
                                    .addOnFailureListener { exception ->
                                        Log.w("Error", "Error getting documents: ", exception)
                                    }
                            }
                            .addOnFailureListener { exception ->
                                Log.w("Error", "Error getting documents: ", exception)
                            }

                    }
                    .addOnFailureListener { exception ->
                        Log.w("Error", "Error getting documents: ", exception)
                    }

            }
            .addOnFailureListener { exception ->
                Log.w("Error", "Error getting documents: ", exception)
            }


    }
}
