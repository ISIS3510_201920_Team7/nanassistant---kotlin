package com.example.myapplication

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.*
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_chat.*
import java.util.HashMap

open class ChatActivity : AppCompatActivity() {

    private lateinit var fotoPerfilX: CircleImageView
    private lateinit var nombreX: TextView
    private lateinit var rvMensajesX: RecyclerView
    private lateinit var txtMensajeX: EditText
    private lateinit var btnEnviarX: Button
    private lateinit var adapterX: AdapterMensajes
    private lateinit var btnEnviarFoto: ImageButton

    //private var database: FirebaseDatabase? = null
    //private var databaseReference: DatabaseReference? = null
    //private var storage: FirebaseStorage? = null
    //private val storageReference: StorageReference? = null
    private var db: FirebaseFirestore? = null
    private var auth: FirebaseAuth? = null
    private var fotoPerfilCadena: String? = null

    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        auth = FirebaseAuth.getInstance()
        db = FirebaseFirestore.getInstance()
        fotoPerfilX = fotoPerfil as CircleImageView
        nombreX = nombre as TextView
        rvMensajesX = rvMensajes as RecyclerView
        txtMensajeX = txtMensaje as EditText
        btnEnviarX = btnEnviar as Button
        //btnEnviarFoto = (ImageButton) findViewById(R.id.btnEnviarFoto);
        nombreX!!.text = auth!!.currentUser!!.displayName
        fotoPerfilCadena = ""

       // database = FirebaseFirestore.getInstance()
        //databaseReference = database!!.getReference("chat")//Sala de chat (nombre)
        //storage = FirebaseStorage.getInstance()

        adapterX = AdapterMensajes(this)
        val l = LinearLayoutManager(this)
        rvMensajes!!.setLayoutManager(l)
        rvMensajes!!.adapter = adapterX

        btnEnviar!!.setOnClickListener {
            //databaseReference.push().setValue(new MensajeEnviar(txtMensaje.getText().toString(),nombre.getText().toString(),fotoPerfilCadena,"1", ServerValue.TIMESTAMP));
            val post = HashMap<String, Any>()
            post["hora"] = FieldValue.serverTimestamp()
            post["mensaje"] = txtMensaje!!.text.toString()
            post["nombre"] = "asistente"
            post["fotoPerfil"] = ""
            post["type_mensaje"] = "1"
            //db.collection("Chat").document("a").collection("gg").add(new MensajeEnviar(txtMensaje.getText().toString(),nombre.getText().toString(),fotoPerfilCadena,"1"));
            db!!.collection("Chat").document("a").collection("gg").add(post)

            txtMensaje!!.setText("")
        }





        adapterX!!.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                super.onItemRangeInserted(positionStart, itemCount)
                setScrollbar()
            }
        })


        db!!.collection("Chat").document("a").collection("gg")
                .addSnapshotListener { snapshots, e ->
                    if (e != null) {
                        Log.w("TAG", "listen:error", e)
                        return@addSnapshotListener
                    }

                    for (dc in snapshots!!.documentChanges) {
                        when (dc.type) {
                            DocumentChange.Type.ADDED -> {Log.d("TAG", "New city: ${dc.document.data}")
                                Log.d("", "New city: " + dc.document.data)
                                //MensajeRecibir mensaje = new MensajeRecibir();
                                val mensaje = dc.document.toObject<MensajeRecibir>(MensajeRecibir::class.java!!)
                                adapterX!!.addMensaje(mensaje)}
                            DocumentChange.Type.MODIFIED -> {Log.d("TAG", "Modified city: ${dc.document.data}")}
                            DocumentChange.Type.REMOVED -> {Log.d("TAG", "Removed city: ${dc.document.data}")}
                        }
                    }
                }

    }

    private fun setScrollbar() {
        rvMensajes!!.scrollToPosition(adapterX!!.itemCount - 1)
    }

    companion object {
        private val PHOTO_SEND = 1
        private val PHOTO_PERFIL = 2
    }


}
