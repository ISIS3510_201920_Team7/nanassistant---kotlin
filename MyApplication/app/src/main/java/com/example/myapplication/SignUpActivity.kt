package com.example.myapplication


import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.models.PeticionesDentroDeCasa
import com.example.myapplication.models.PeticionesFueraDeCasa
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    private lateinit var db : FirebaseFirestore

    private var valido : Boolean = true

    private var isConnected : Boolean = true

    private var lista = arrayListOf<String>()

    private var peticiones = arrayListOf<PeticionesDentroDeCasa>()

    private var peticiones2 = arrayListOf<PeticionesFueraDeCasa>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        auth = FirebaseAuth.getInstance()
        lista = arrayListOf("Lunes", "Martes", "Miércoles", "Jueves")
        lista.clear()
        db = FirebaseFirestore.getInstance()
        registrar.setOnClickListener {

            signUpUser()
        }
        //var btn_Registrar = findViewById(R.id.Registrar)

        boton_atras.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }

    }

    private fun checkConnectivity() : Boolean{
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting
        if(!isConnected){
            Toast.makeText(this, "Por favor revisar su conexión a internet.",Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }

    fun signUpUser(){
        if(nombre.text.toString().isEmpty()){
            nombre.error  ="Por favor ingresar el nombre de usuario"
            nombre.requestFocus()
            valido = false
        }

        if(correo.text.toString().isEmpty()){
            correo.error  ="Por favor ingresar el correo"
            correo.requestFocus()
            valido = false
        }

        /*if(Patterns.EMAIL_ADDRESS.matcher(correo.text.toString()).matches()){
            correo.error = "Por favor colocar un correo válido"
            correo.requestFocus()
        }*/

        if(clave_registro.text.toString().isEmpty()){
            clave_registro.error  ="Por favor ingresar la clave"
            clave_registro.requestFocus()
            valido = false
        }

        if(celular_registro.text.toString().isEmpty()){
            celular_registro.error  ="Por favor ingresar el número de Celular"
            celular_registro.requestFocus()
            valido = false
        }

        if(documento_registro.text.toString().isEmpty()){
            documento_registro.error  ="Por favor ingresar el documento de identidad"
            documento_registro.requestFocus()
            valido = false
        }

        if(valido) {
            if (checkConnectivity()) {
                this.auth.createUserWithEmailAndPassword(correo.text.toString(), clave_registro.text.toString())
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            logIn(correo.text.toString(), clave_registro.text.toString())
                            val data = HashMap<String, String>()
                            data.put("Nombre", nombre.text.toString())
                            data.put("Correo", correo.text.toString())
                            data.put("Clave", clave_registro.text.toString())
                            data.put("Celular", celular_registro.text.toString())
                            data.put("Documento", documento_registro.text.toString())
                            val user = FirebaseAuth.getInstance().currentUser
                            if (user != null) {
                                val reference = db.collection("Asistentes").document(user.uid)
                                reference.set(data)
                            } else {

                            }
                            Log.e("REGISTER", "Entro en el método")
                            traerServicios()

                            //var intento = Intent(this, PeticionesActivity::class.java)
                            //startActivity(intento)

                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(
                                baseContext, "El Registro falló. Por favor intente más tarde.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                        // ...
                    }
            } else {
                Toast.makeText(
                    baseContext, "El Registro falló por temas de conexión.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        else{
            Toast.makeText(
                baseContext, "El Registro falló por falta de información en el registro",
                Toast.LENGTH_SHORT
            ).show()
        }

        valido = true
    }

    public fun traerServicios(){
        var intento = Intent(this, ListaActivity::class.java)

        db = FirebaseFirestore.getInstance()

        auth = FirebaseAuth.getInstance()
        var idUsuario = "a"

        idUsuario = auth.currentUser?.uid ?: "a"


        db.collection("PeticionesDentroDeCasa").whereEqualTo("asistente","Sin asignar")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    Log.d("Encontró documento", "${document.id} => ${document.data}")
                    val nuevaPeticion : PeticionesDentroDeCasa
                    nuevaPeticion = document.toObject(PeticionesDentroDeCasa::class.java)
                    nuevaPeticion.identificador = document.id
                    peticiones.add(nuevaPeticion)
                    lista.add("\n"+"Categoria: " +nuevaPeticion.categoria+"\n" +"fecha: "+nuevaPeticion.fecha+"\n")
                    //intento.putExtra("dentrodecasa",peticiones)


                }
                db.collection("PeticionesDentroDeCasa").whereEqualTo("asistente",idUsuario)
                    .get()
                    .addOnSuccessListener { documents ->
                        for (document in documents) {
                            Log.d("Encontró documento", "${document.id} => ${document.data}")
                            val nuevaPeticion : PeticionesDentroDeCasa
                            nuevaPeticion = document.toObject(PeticionesDentroDeCasa::class.java)
                            nuevaPeticion.identificador = document.id
                            peticiones.add(nuevaPeticion)
                            lista.add("\n"+"Categoria: " +nuevaPeticion.categoria+"\n" +"fecha: "+nuevaPeticion.fecha+"\n")
                        }
                        intento.putExtra("dentrodecasa",peticiones)
                        //intento.putExtra("ListadeSalidas", lista)


                db.collection("PeticionesFueraDeCasa").whereEqualTo("asistente","Sin asignar")
                    .get()
                    .addOnSuccessListener { documents ->
                        for (document in documents) {
                            Log.d("Encontró documento", "${document.id} => ${document.data}")
                            val nuevaPeticion : PeticionesFueraDeCasa
                            nuevaPeticion = document.toObject(PeticionesFueraDeCasa::class.java)
                            nuevaPeticion.identificador = document.id
                            peticiones2.add(nuevaPeticion)
                            lista.add("\n"+"Categoria: " +nuevaPeticion.categoria+"\n" +"fecha: "+nuevaPeticion.fecha+"\n")
                        }
                        //intento.putExtra("fueradecasa",peticiones2)
                        //intento.putExtra("ListadeSalidas", lista)


                        db.collection("PeticionesFueraDeCasa").whereEqualTo("asistente",idUsuario)
                            .get()
                            .addOnSuccessListener { documents ->
                                for (document in documents) {
                                    Log.d("Encontró documento", "${document.id} => ${document.data}")
                                    val nuevaPeticion : PeticionesFueraDeCasa
                                    nuevaPeticion = document.toObject(PeticionesFueraDeCasa::class.java)
                                    nuevaPeticion.identificador = document.id
                                    peticiones2.add(nuevaPeticion)
                                    lista.add("\n"+"Categoria: " +nuevaPeticion.categoria+"\n" +"fecha: "+nuevaPeticion.fecha+"\n")
                                }
                                intento.putExtra("fueradecasa",peticiones2)
                                intento.putExtra("ListadeSalidas", lista)

                                startActivity(intento)
                                finish()
                            }
                            .addOnFailureListener { exception ->
                                Log.w("Error", "Error getting documents: ", exception)
                            }
                    }
                    .addOnFailureListener { exception ->
                        Log.w("Error", "Error getting documents: ", exception)
                    }

            }
            .addOnFailureListener { exception ->
                Log.w("Error", "Error getting documents: ", exception)
            }

            }
            .addOnFailureListener { exception ->
                Log.w("Error", "Error getting documents: ", exception)
            }


    }

    private fun logIn(correo: String, contrasenia: String){
        auth.signInWithEmailAndPassword(correo, contrasenia)
        .addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                // Sign in success, update UI with the signed-in user's information
                Log.d("login", "signInWithEmail:success")

            } else {
                // If sign in fails, display a message to the user.
                Log.w("falló login", "signInWithEmail:failure", task.exception)
                Toast.makeText(baseContext, "Authentication failed.",
                    Toast.LENGTH_SHORT).show()

            }

            // ...
        }
    }


}