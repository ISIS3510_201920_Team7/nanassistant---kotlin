package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.firestore.FirebaseFirestore

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    var mMap: GoogleMap?=null
    var longitud:Double=0.0
    var latitud: Double=0.0
    var usuario: String =""
    lateinit var marcador: Marker
    private lateinit var db : FirebaseFirestore


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        longitud = intent.getStringExtra("longitud").toDouble()
        latitud = intent.getStringExtra("longitud").toDouble()
        usuario = intent.getStringExtra("usuario")
        db = FirebaseFirestore.getInstance()


    }

    /**z
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        var coordenadas = LatLng(latitud.toDouble(),longitud.toDouble())
        marcador = mMap!!.addMarker(MarkerOptions().position(coordenadas).title("Usuario"))
        mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(coordenadas,16.0f))


        //agregarMarcador(longitud,latitud)
        val docRef = db.collection("localizacion").document(usuario)
        docRef.addSnapshotListener { snapshot, e ->
            if (e != null) {
                Log.w("", "Listen failed.", e)
                return@addSnapshotListener
            }

            if (snapshot != null && snapshot.exists()) {
                Log.d("", "Current data: ${snapshot.data}")

                //agregarMarcador(snapshot.data!!.get("longitud").toString().toDouble(),snapshot.data!!.get("latitud").toString().toDouble())
                var coordenadas = LatLng(snapshot.data!!.get("latitud").toString().toDouble(),snapshot.data!!.get("longitud").toString().toDouble())
                Log.e("asad",mMap.toString())
                mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(coordenadas,16.0f))
                if(marcador!=null){
                    marcador.remove()
                }

                marcador = mMap!!.addMarker(MarkerOptions().position(coordenadas).title("Usuario"))
            } else {
                Log.d("", "Current data: null")
            }
        }
    }

    fun agregarMarcador(longitud:Double, latitud:Double){
        var coordenadas = LatLng(latitud,longitud)
        Log.e("asad",mMap.toString())
        mMap?.moveCamera(CameraUpdateFactory.newLatLng(coordenadas))
        //marcador.remove()
       // marcador = nMap.addMarker(MarkerOptions().position(coordenadas).title("Usuario"))


    }


}
