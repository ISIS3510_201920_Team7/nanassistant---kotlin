package com.example.myapplication

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import java.text.SimpleDateFormat
import java.util.ArrayList

class AdapterMensajes(private val c: Context) : RecyclerView.Adapter<HolderMensaje>() {



    private val listMensaje = ArrayList<MensajeRecibir>()
    override fun getItemCount(): Int {
        return listMensaje.size
    }


    fun addMensaje(m: MensajeRecibir) {
        listMensaje.add(m)
        notifyItemInserted(listMensaje.size)
    }

    fun actualizarMensaje(m: MensajeRecibir) {
        listMensaje[listMensaje.size - 1] = m
        notifyItemChanged(listMensaje.size - 1)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderMensaje {
        val v = LayoutInflater.from(c).inflate(R.layout.card_view_mensajes, parent, false)
        return HolderMensaje(v)
    }

    override fun onBindViewHolder(holder: HolderMensaje, position: Int) {

        if (listMensaje[position].getHora()!= null) {
            holder.nombre!!.setText(listMensaje[position].getNombre())
            holder.mensaje!!.setText(listMensaje[position].getMensaje())
            if (listMensaje[position].getType_mensaje().equals("1")) {
                holder.fotoMensaje!!.setVisibility(View.VISIBLE)
                holder.mensaje!!.setVisibility(View.VISIBLE)
                Glide.with(c).load(listMensaje[position].getUrlFoto()).into(holder.fotoMensaje)
            } else if (listMensaje[position].getType_mensaje().equals("1")) {
              //  holder.fotoMensaje!!.setVisibility(View.GONE)
              //  holder.fotoMensaje!!.setVisibility(View.VISIBLE)
            }
            /*
            if (listMensaje[position].getFotoPerfil()!!.isEmpty()) {
                holder.fotoMensaje!!.setImageResource(R.drawable.ic_perfil)
            } else {
                Glide.with(c).load(listMensaje[position].getFotoPerfil()).into(holder.fotoMensajePerfil
                )
            }

             */
            if (listMensaje[position].getHora() != null) {
                val d = listMensaje[position].getHora()
                val sdf = SimpleDateFormat("EEE dd, hh:mm:ss a")//a pm o am
                holder.hora!!.setText(sdf.format(d!!.toDate()))
            }
        }
    }

}