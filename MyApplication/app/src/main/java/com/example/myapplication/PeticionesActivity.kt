package com.example.myapplication



import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.models.PeticionesDentroDeCasa
import com.example.myapplication.models.PeticionesFueraDeCasa

import com.firebase.ui.firestore.FirestoreRecyclerOptions

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_peticiones.*


class PeticionesActivity : AppCompatActivity() {
    private val db = FirebaseFirestore.getInstance()
    private val notebookRef = db.collection("PeticionesDentroDeCasa")
    private var auth: FirebaseAuth? = null

    private var adapter: SalidaAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        auth = FirebaseAuth.getInstance()
        setContentView(R.layout.activity_peticiones)
        setUpRecyclerView()
    }

    private fun setUpRecyclerView() {


        Log.e("correo", "aa")
        val query = notebookRef.whereEqualTo("correo", "carlosmendozappp@gmail.com")

        val options = FirestoreRecyclerOptions.Builder<PeticionesFueraDeCasa>()
                .setQuery(query, PeticionesFueraDeCasa::class.java)
                .build()
        Log.e("xxxxx", "1")
        adapter = SalidaAdapter(options)
        Log.e("xxxxx", "2")

        val recyclerView = recycler_view as RecyclerView
        recyclerView.setHasFixedSize(true)
        recyclerView.setLayoutManager(LinearLayoutManager(this))
        recyclerView.setAdapter(adapter)

        adapter!!.setOnItemClickListener(object : SalidaAdapter.OnItemClickListener {
            override fun onItemClick(documentSnapshot: DocumentSnapshot, position: Int) {
                val note = documentSnapshot.toObject(PeticionesDentroDeCasa::class.java)
                val id = documentSnapshot.id
                val path = documentSnapshot.reference.path
                //Toast.makeText(PeticionesActivity.this,
                //   "Position: " + position + " ID: " + id+ "fecha"+ documentSnapshot.getData().get("fecha"), Toast.LENGTH_SHORT).show();

                val intent = Intent(this@PeticionesActivity, DetalleActivity::class.java)
                val a = documentSnapshot.data
                val direccion1 = a!!["direccion1"]!!.toString()
                val fecha = a["fecha"]!!.toString()
                val hora = a["hora"]!!.toString()
                val categoria = a["categoria"]!!.toString()
                val descripcion = a["descripcion"]!!.toString()
                val estado = a["estado"]!!.toString()
                //                String identificador = a.get("identificador").toString();;
                val asistente = a["asistente"]!!.toString()


                intent.putExtra("dir1", direccion1)
                intent.putExtra("fecha", fecha)
                intent.putExtra("hora", hora)
                intent.putExtra("cat", categoria)
                intent.putExtra("des", descripcion)
                intent.putExtra("estado", estado)
                //intent.putExtra("identificador",identificador);
                //intent.putExtra("dentrodecasa",a);
                intent.putExtra("asistente", asistente)
                this@PeticionesActivity.startActivity(intent)


            }
        })
    }

    protected override fun onStart() {
        super.onStart()
        adapter!!.startListening()
    }



    protected override fun onStop() {
        super.onStop()
        adapter!!.stopListening()
    }
}

