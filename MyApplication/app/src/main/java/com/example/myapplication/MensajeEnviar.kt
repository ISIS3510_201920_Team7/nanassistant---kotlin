package com.example.myapplication

open class MensajeEnviar {

    private var mensaje: String? = null
    private var urlFoto: String? = null
    private var nombre: String? = null
    private var fotoPerfil: String? = null
    private var type_mensaje: String? = null

    fun MensajeEnviar() {}

    fun MensajeEnviar(mensaje: String, nombre: String, fotoPerfil: String, type_mensaje: String) {
        this.mensaje = mensaje
        this.nombre = nombre
        this.fotoPerfil = fotoPerfil
        this.type_mensaje = type_mensaje
    }

    fun MensajeEnviar(mensaje: String, urlFoto: String, nombre: String, fotoPerfil: String, type_mensaje: String) {
        this.mensaje = mensaje
        this.urlFoto = urlFoto
        this.nombre = nombre
        this.fotoPerfil = fotoPerfil
        this.type_mensaje = type_mensaje
    }

    fun getMensaje(): String? {
        return mensaje
    }

    fun setMensaje(mensaje: String) {
        this.mensaje = mensaje
    }

    fun getNombre(): String? {
        return nombre
    }

    fun setNombre(nombre: String) {
        this.nombre = nombre
    }

    fun getFotoPerfil(): String? {
        return fotoPerfil
    }

    fun setFotoPerfil(fotoPerfil: String) {
        this.fotoPerfil = fotoPerfil
    }

    fun getType_mensaje(): String? {
        return type_mensaje
    }

    fun setType_mensaje(type_mensaje: String) {
        this.type_mensaje = type_mensaje
    }

    fun getUrlFoto(): String? {
        return urlFoto
    }

    fun setUrlFoto(urlFoto: String) {
        this.urlFoto = urlFoto
    }

}