package com.example.myapplication

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.widget.AdapterView
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.models.PeticionesDentroDeCasa
import com.example.myapplication.models.PeticionesFueraDeCasa
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_sign_up.*
import android.widget.ArrayAdapter
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_lista.*

class ListaActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    private lateinit var db : FirebaseFirestore

    private var isConnected : Boolean = true

    private var valido : Boolean = true


    lateinit var homeFragment: HomeFragment

    lateinit var listaFragment: ListaFragment

    lateinit var profileFragment: ProfileFragment

    lateinit var aceptadosFragment: AceptadosFragment

    lateinit var disponiblesFragment: DisponiblesFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista)

        homeFragment = HomeFragment()
        listaFragment = ListaFragment()

        val bottomNavigation : BottomNavigationView = findViewById(R.id.btm_nav)

        homeFragment = HomeFragment()
        aceptadosFragment = AceptadosFragment()
        disponiblesFragment = DisponiblesFragment()

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.frame_layout, disponiblesFragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit()

        bottomNavigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.home ->{
                    aceptadosFragment = AceptadosFragment()
                    supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.frame_layout, aceptadosFragment)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .commit()
                }
                R.id.lista ->{
                    disponiblesFragment = DisponiblesFragment()
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame_layout, disponiblesFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit()
                }

                R.id.profile ->{
                    profileFragment = ProfileFragment()
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame_layout, profileFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit()
                }

            }
            true
        }



    }

    private fun checkConnectivity(){
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting
        if(!isConnected){
            Toast.makeText(this, "Por favor revisar su conexión a internet.",Toast.LENGTH_SHORT).show()
        }
    }

}