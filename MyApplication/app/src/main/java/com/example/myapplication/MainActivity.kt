package com.example.myapplication

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    private lateinit var db : FirebaseFirestore

    private var isConnected : Boolean = true

    private var valido : Boolean = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()
        /*if(auth.currentUser!=null)
        {
            startActivity(Intent(this,ListaActivity::class.java))
            finish()
        }*/

        Register.setOnClickListener {
            startActivity(Intent(this,SignUpActivity::class.java))
            showRegistration()
            finish()
        }

        Login.setOnClickListener{
            logIn()
        }


    }

    private fun checkConnectivity(){
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting
        if(!isConnected){
            Toast.makeText(this, "Por favor revisar su conexión a internet.",Toast.LENGTH_SHORT).show()
        }
    }

    private fun logIn(){
        if(correo_login.text.toString().isEmpty()){
            correo_login.error  ="Por favor ingresar el correo"
            correo_login.requestFocus()
            valido = false
        }

        if(clave_login.text.toString().isEmpty()){
            clave_login.error  ="Por favor ingresar la clave"
            clave_login.requestFocus()
            valido = false
        }

        if(valido==true) {
            checkConnectivity()
            if (isConnected == true) {
                auth.signInWithEmailAndPassword(correo_login.text.toString(), clave_login.text.toString())
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            val user = auth.currentUser
                        } else {
                            Toast.makeText(
                                baseContext, "Falló la autenticación 1",
                                Toast.LENGTH_SHORT
                            ).show()
                            updateUI(null)
                        }
                        if(auth.currentUser!=null)
                        {
                            startActivity(Intent(this,ListaActivity::class.java))
                            finish()
                        }

                        // ...
                    }
            } else {
                Toast.makeText(
                    baseContext, "El Login falló por temas de conexión.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        else{
            Toast.makeText(
                baseContext, "El Login falló por falta de información en el login",
                Toast.LENGTH_SHORT
            ).show()
        }

        valido=true
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        //updateUI(currentUser)
    }

    private fun updateUI(currentUser : FirebaseUser?){
        if(currentUser!= null){
            startActivity(Intent(this,ListaActivity::class.java))
            //startActivity(Intent(this,PeticionesActivity::class.java))
            finish()
        }
        else {
            Toast.makeText(
                baseContext, "Falló la autenticación 2",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun showRegistration(){
        registration_layout.visibility= View.VISIBLE
        login_layout.visibility=View.GONE
        home.visibility=View.GONE
    }


}
