package com.example.myapplication


import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import androidx.core.content.ContextCompat.getSystemService
import com.example.myapplication.models.PeticionesDentroDeCasa
import com.example.myapplication.models.PeticionesFueraDeCasa
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_lista.*
import java.lang.Exception


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class HomeFragment : Fragment() {

    private var lista = arrayListOf<String>()

    private var peticiones = arrayListOf<PeticionesDentroDeCasa>()

    private var peticiones2 = arrayListOf<PeticionesFueraDeCasa>()

    private var isConnected : Boolean = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var arrayAdapter : ArrayAdapter<String> = ArrayAdapter(activity, android.R.layout.simple_list_item_1, arguments?.getStringArrayList("Lista"))

        lvSalidas.adapter = arrayAdapter

        var lista2 : ListView = activity?.findViewById(R.id.lvSalidas) ?: try {
            lista = activity?.intent?.getStringArrayListExtra("ListadeSalidas") as ArrayList<String>

        } catch(ex: Exception) {
            lista = arrayListOf("")
            Log.e("ERROR",ex.toString())
        } as ListView
        peticiones = activity?.intent?.getSerializableExtra("dentrodecasa") as ArrayList<PeticionesDentroDeCasa>
        peticiones2 =  activity?.intent?.getSerializableExtra("fueradecasa") as ArrayList<PeticionesFueraDeCasa>


        lvSalidas.setOnItemClickListener { parent, view, position, id ->
            //checkConnectivity()
            //if(isConnected==true) {
            if (position <= peticiones.size - 1) {
                var peticion = peticiones.get(position)
                val direccion1 = peticion.direccion1
                val fecha = peticion.fecha
                val hora = peticion.hora
                val categoria = peticion.categoria
                val descripcion = peticion.descripcion
                val estado = peticion.estado
                val identificador = peticion.identificador
                var intento = Intent(activity, DetalleActivity2::class.java)
                intento.putExtra("dir1", direccion1)
                intento.putExtra("fecha", fecha)
                intento.putExtra("hora", hora)
                intento.putExtra("cat", categoria)
                intento.putExtra("des", descripcion)
                intento.putExtra("estado", estado)
                intento.putExtra("identificador", identificador)
                intento.putExtra("dentrodecasa", peticion)
                startActivity(intento)
            } else {
                var peticion = peticiones2.get(position)
                val direccion1 = peticion.direccion1
                val direccion2 = peticion.direccion2
                val fecha = peticion.fecha
                val hora = peticion.hora
                val categoria = peticion.categoria
                val descripcion = peticion.descripcion
                val estado = peticion.estado
                val identificador = peticion.identificador
                var intent = Intent(activity, DetalleActivity2::class.java)
                intent.putExtra("dir1", direccion1)
                intent.putExtra("dir2", direccion2)
                intent.putExtra("fecha", fecha)
                intent.putExtra("hora", hora)
                intent.putExtra("cat", categoria)
                intent.putExtra("des", descripcion)
                intent.putExtra("estado", estado)
                intent.putExtra("identificador", identificador)
                intent.putExtra("fueradecasa", peticion)
                startActivity(intent)
            }
            //Toast.makeText(activity, "Clicked item :" + " " + position, Toast.LENGTH_SHORT).show()

            //}
            //else{
            //    Toast.makeText(activity, "Por favor revisar su conexión a internet.", Toast.LENGTH_SHORT).show()
            //}

        }

    }



}
