package com.example.myapplication
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.example.myapplication.R.id

import de.hdodenhof.circleimageview.CircleImageView


class HolderMensaje(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var nombre: TextView? = null
    var mensaje: TextView? = null
    var hora: TextView? = null
    var fotoMensajePerfil: CircleImageView? = null
    var fotoMensaje: ImageView? = null

    init {

        nombre = itemView.findViewById(id.nombreMensaje) as TextView
        mensaje = itemView.findViewById(id.mensajeMensaje) as TextView
        hora = itemView.findViewById(id.horaMensaje) as TextView
        fotoMensajePerfil = itemView.findViewById(id.fotoPerfilMensaje) as CircleImageView
        fotoMensaje = itemView.findViewById(id.mensajeFoto) as ImageView
    }
}