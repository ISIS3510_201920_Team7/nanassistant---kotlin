package com.example.myapplication


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.models.PeticionesDentroDeCasa
import com.example.myapplication.models.PeticionesFueraDeCasa

import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.DocumentSnapshot

class SalidaAdapter(@NonNull options: FirestoreRecyclerOptions<PeticionesFueraDeCasa>) : FirestoreRecyclerAdapter<PeticionesFueraDeCasa, SalidaAdapter.SalidaHolder>(options) {

    private var listener: OnItemClickListener? = null

    protected override fun onBindViewHolder(@NonNull holder: SalidaHolder, position: Int, @NonNull model: PeticionesFueraDeCasa) {
        holder.textViewTitle.setText(model.categoria)
        holder.textViewDescription.setText(model.fecha)
        holder.textViewPriority.setText(model.estado)
    }

    @NonNull
    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): SalidaHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.salida_item,
                parent, false)
        return SalidaHolder(v)
    }

    inner class SalidaHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textViewTitle: TextView
        var textViewDescription: TextView
        var textViewPriority: TextView

        init {
            textViewTitle = itemView.findViewById(R.id.text_view_title)
            textViewDescription = itemView.findViewById(R.id.text_view_description)
            textViewPriority = itemView.findViewById(R.id.text_view_priority)

            itemView.setOnClickListener {
                val position = getAdapterPosition()
                if (position != RecyclerView.NO_POSITION && listener != null) {
                    listener!!.onItemClick(getSnapshots().getSnapshot(position), position)
                }
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(documentSnapshot: DocumentSnapshot, position: Int)
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        this.listener = listener
    }
}