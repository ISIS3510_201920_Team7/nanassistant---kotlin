package com.example.myapplication


import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import com.example.myapplication.models.PeticionesDentroDeCasa
import com.example.myapplication.models.PeticionesFueraDeCasa
import kotlinx.android.synthetic.main.fragment_lista.*
import kotlin.Exception as Exception1


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class ListaFragment : Fragment() {

    private var lista = arrayListOf<String>()

    private var peticiones = arrayListOf<PeticionesDentroDeCasa>()

    private var peticiones2 = arrayListOf<PeticionesFueraDeCasa>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_lista, container, false)
    }


    @SuppressLint("LongLogTag")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var arrayAdapter : ArrayAdapter<String> = ArrayAdapter(activity, android.R.layout.simple_list_item_1, arguments?.getStringArrayList("Lista2"))

        lvSalidas2.adapter = arrayAdapter

        var lista2 : ListView = activity?.findViewById(R.id.lvSalidas2) ?: try {
        lista = activity?.intent?.getStringArrayListExtra("ListadeSalidas") as ArrayList<String>

        } catch(ex:Exception) {
            lista = arrayListOf("")
            Log.e("ERROR",ex.toString())
        } as ListView
        peticiones = activity?.intent?.getSerializableExtra("dentrodecasa") as ArrayList<PeticionesDentroDeCasa>
        peticiones2 =  activity?.intent?.getSerializableExtra("fueradecasa") as ArrayList<PeticionesFueraDeCasa>


        lvSalidas2.setOnItemClickListener { parent, view, position, id ->
            //checkConnectivity()
            //if(isConnected==true) {
                if (position <= peticiones.size - 1) {
                    var peticion = peticiones.get(position)
                    val direccion1 = peticion.direccion1
                    val fecha = peticion.fecha
                    val hora = peticion.hora
                    val categoria = peticion.categoria
                    val descripcion = peticion.descripcion
                    val estado = peticion.estado
                    val identificador = peticion.identificador
                    val nombre = peticion.usuario
                    var intento = Intent(activity, DetalleActivity::class.java)
                    intento.putExtra("dir1", direccion1)
                    intento.putExtra("fecha", fecha)
                    intento.putExtra("hora", hora)
                    intento.putExtra("cat", categoria)
                    intento.putExtra("des", descripcion)
                    intento.putExtra("estado", estado)
                    intento.putExtra("identificador", identificador)
                    intento.putExtra("dentrodecasa", peticion)
                    intento.putExtra("nombre",nombre)
                    startActivity(intento)
                } else {
                    var peticion = peticiones2.get(position)
                    val direccion1 = peticion.direccion1
                    val direccion2 = peticion.direccion2
                    val fecha = peticion.fecha
                    val hora = peticion.hora
                    val categoria = peticion.categoria
                    val descripcion = peticion.descripcion
                    val estado = peticion.estado
                    val identificador = peticion.identificador
                    val nombre = peticion.usuario
                    var intent = Intent(activity, DetalleActivity::class.java)
                    intent.putExtra("dir1", direccion1)
                    intent.putExtra("dir2", direccion2)
                    intent.putExtra("fecha", fecha)
                    intent.putExtra("hora", hora)
                    intent.putExtra("cat", categoria)
                    intent.putExtra("des", descripcion)
                    intent.putExtra("estado", estado)
                    intent.putExtra("identificador", identificador)
                    intent.putExtra("fueradecasa", peticion)

                    startActivity(intent)
                }
                //Toast.makeText(activity, "Clicked item :" + " " + position, Toast.LENGTH_SHORT).show()

            //}
            //else{
            //    Toast.makeText(activity, "Por favor revisar su conexión a internet.", Toast.LENGTH_SHORT).show()
            //}

        }





    }

}
