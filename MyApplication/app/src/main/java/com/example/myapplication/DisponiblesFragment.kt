package com.example.myapplication

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.models.PeticionesFueraDeCasa
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_peticiones.*




class DisponiblesFragment : androidx.fragment.app.Fragment() {
    // TODO: Rename and change types of parameters
    private var adapter: SalidaAdapter? = null
    private lateinit var auth: FirebaseAuth



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        auth = FirebaseAuth.getInstance()
        return inflater.inflate(R.layout.fragment_aceptados, container, false)
    }
    @SuppressLint("LongLogTag")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val db = FirebaseFirestore.getInstance()
        val notebookRef = db.collection("Peticiones")
        val query = notebookRef.whereEqualTo("asistente", "Sin asignar")
        val options = FirestoreRecyclerOptions.Builder<PeticionesFueraDeCasa>()
                .setQuery(query, PeticionesFueraDeCasa::class.java)
                .build()
        adapter = SalidaAdapter(options)
        val recyclerView = recycler_view as RecyclerView
        recyclerView.setHasFixedSize(true)
        recyclerView.setLayoutManager(LinearLayoutManager(activity))
        recyclerView.setAdapter(adapter)

        adapter!!.setOnItemClickListener(object : SalidaAdapter.OnItemClickListener {
            override fun onItemClick(documentSnapshot: DocumentSnapshot, position: Int) {


                val a = documentSnapshot.data
                var intent = Intent(activity, DetalleActivity::class.java)
                intent.putExtra("identificador",documentSnapshot.id)
                intent.putExtra("fueradecasa",documentSnapshot.toObject(PeticionesFueraDeCasa::class.java))
                if(documentSnapshot.toObject(PeticionesFueraDeCasa::class.java)==null){
                    Log.e("XXXXX","aqui")
                }

                if (a!!["direccion2"] != null &&  a!!["direccion2"] != "") {

                    Log.e("NUMERO INTENTO",a!!["direccion2"].toString())
                    intent = Intent(activity, DetalleActivity::class.java)
                    intent.putExtra("dir2", a!!["direccion2"]!!.toString())

                    intent.putExtra("identificador",documentSnapshot.id)

                }


                Log.e("aaaaa", a.keys.toString())
                val direccion1 = a!!["direccion1"]!!.toString()
                val fecha = a["fecha"]!!.toString()
                val hora = a["hora"]!!.toString()
                val categoria = a["categoria"]!!.toString()
                val descripcion = a["descripcion"]!!.toString()
                val estado = a["estado"]!!.toString()
                val asistente = a["asistente"]!!.toString()
                val correo = a["correo"]!!.toString()
                val usuario = a["usuario"]!!.toString()


                intent.putExtra("dir1", direccion1)
                intent.putExtra("fecha", fecha)
                intent.putExtra("hora", hora)
                intent.putExtra("cat", categoria)
                intent.putExtra("des", descripcion)
                intent.putExtra("estado", estado)
                intent.putExtra("asistente", asistente)
                intent.putExtra("correo", correo)
                intent.putExtra("usuario", usuario)
                activity!!.startActivity(intent)


            }
        })
    }
    override fun onStart() {
        super.onStart()
        adapter!!.startListening()
    }
    @Override
    override fun onStop() {
        super.onStop()
        adapter!!.stopListening()
    }


}
